TCPlivePLOT - realtime-visualization of TCP flows
================================================================================
Realtime-visualization of TCP flows logged by TCPlog.

![TCPlivePLOT screenrecording](screenrecording.gif)


Based on
--------------------------------------------------------------------------------
TCPlivePLOT is written in Python3 and tested on GNU/Linux 4.{1-15}.

For TCPlog see https://git.scc.kit.edu/CPUnetLOG/TCPlog/


Requirements TCPlivePLOT:
--------------------------------------------------------------------------------
* python3
* matplotlib (1.4.X recommended)
* numpy
* matplotlib-interactive backend

* recommended backends:
    * Qt4Agg or
    * Qt5Agg


Running TCPlivePLOT:
--------------------------------------------------------------------------------
* ./tcpliveplot.py OR
* tcpliveplot (after installation)


Installation of TCPlivePLOT:
--------------------------------------------------------------------------------
* via pip3 (from Repo):
    * sudo pip3 install .               # system-wide installation
    * pip3 install --user .             # local installation
* via pip3 (from PyPi):
    * sudo pip3 install tcpliveplot     # system-wide installation
    * pip3 install --user tcpliveplot   # local installation


Notes:
--------------------------------------------------------------------------------
* Installation on Ubuntu 16.04 requires the following additional packages:
    * libfreetype6-dev
    * libxft-dev
    * python3-pyside
* A matplotlibrc with an interactive backend is mandatory:
    * "matplotlibrc.example" has to be placed here: ~/.config/matplotlib/matplotlibrc
* Binaries installed via pip3 (local installation) are usually located in ~/.local/bin ($PATH may has
        to be extended)
