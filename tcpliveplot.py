#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Convenience wrapper for running TCPlivePLOT directly from source tree."""

from tcpliveplot.main import main

if __name__ == '__main__':
    main()
